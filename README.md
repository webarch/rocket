# Ansible Role to install Rocket.Chat

This role [installs Rocket.Chat using
snaps](https://docs.rocket.chat/installing-and-updating/snaps), it has been
written with the intention of using it with an Apache reverse proxy, for
example:

```apache
# ProxyPass "/" "ws://127.0.0.1:3000/"
ProxyPass "/" "http://127.0.0.1:3000/"
RewriteEngine on
RewriteCond %{HTTP:Upgrade} websocket [NC]
RewriteCond %{HTTP:Connection} upgrade [NC]
RewriteRule ^/?(.*) "ws://127.0.0.1:3000/$1" [P,L]
ProxyPassReverse "/" "http://127.0.0.1:3000/"
```
This can be generated using the [users role](https://git.coop/webarch/users) and:

```yml
        users_apache_proxy_pass:
          - path: /
            url: http://127.0.0.1:3000/
            rewrite_conditions:
              - '%{HTTP:Upgrade} websocket [NC]'
              - '%{HTTP:Connection} upgrade [NC]'
            rewrite_rules:
              - '^/?(.*) "ws://127.0.0.1:3000/$1" [P,L]'
            reverse: true
```

